import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    mr = df[df['Name'].str.startswith('Mr.)][['Name','Age']]
    mrs = df[df['Name'].str.startswith('Mrs.)][['Name','Age']]
    miss = df[df['Name'].str.startswith('Miss.)][['Name','Age']]
    x = mr['Age'].isna().sum()
    y = mr['Age'].median()
    k = mrs['Age'].isna().sum()
    m = mrs['Age'].median()
    l = miss['Age'].isna().sum()
    n = miss['Age'].median() 
    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
